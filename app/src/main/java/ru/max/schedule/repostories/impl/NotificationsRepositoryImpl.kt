package ru.max.schedule.repostories.impl

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.max.schedule.await
import ru.max.schedule.repostories.NotificationsRepository

// Name of collection on Firestore
private const val NOTIFICATIONS_COLLECTION = "notifications"

class NotificationsRepositoryImpl(
    private val firestore: FirebaseFirestore
) : NotificationsRepository {

    override suspend fun loadNotifications(uuid: String): QuerySnapshot? = withContext(Dispatchers.IO) {
        return@withContext try {
            firestore.collection(NOTIFICATIONS_COLLECTION).whereEqualTo("uuid", uuid).get().await()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

}