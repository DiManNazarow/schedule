package ru.max.schedule.useCases

import com.google.firebase.firestore.ktx.toObject
import ru.max.schedule.models.NotificationResponseData
import ru.max.schedule.repostories.NotificationsRepository

/**
 * Use case which load notifications from Firestore
 * and convert to internal data model
 * @param notificationsRepository - repository for access notifications on Firestore
 */
class LoadNotificationUseCase(
    private val notificationsRepository: NotificationsRepository
) {

    /**
     * Loads and convert data
     */
    suspend fun loadNotifications(uuid: String): List<NotificationResponseData> {
        // Gets data for firestore
        val data = notificationsRepository.loadNotifications(uuid)
        val notifications = ArrayList<NotificationResponseData>()
        data?.documents?.forEach { doc ->
            val notification = doc.toObject<NotificationResponseData>() ?: throw IllegalStateException("Cannot convert document to Notification model")
            notifications.add(notification)
        }
        return notifications
    }

}