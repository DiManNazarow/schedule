package ru.max.schedule.ui.schedule

import android.content.Context
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import ru.max.schedule.R
import ru.max.schedule.adapters.ScheduleAdapter
import ru.max.schedule.adapters.SpaceItemDecorator
import ru.max.schedule.databinding.FragmentScheduleBinding
import ru.max.schedule.ui.BaseFragment

/**
 * Fragment for showing schedule
 */
class ScheduleFragment : BaseFragment<FragmentScheduleBinding>() {

    private val viewModel: ScheduleViewModel by viewModel()

    private lateinit var adapter: ScheduleAdapter

    override fun getLayout(): Int = R.layout.fragment_schedule

    override fun setupViewModels(context: Context) {
        viewModel.items.observe(this, Observer { items ->
            items?.let {
                adapter.items = it
                binding.refresh.isRefreshing = false
            }
        })
        viewModel.noAuth.observe(this, Observer { noAuth ->
            noAuth?.let { if (noAuth) {
                binding.refresh.isRefreshing = false
                showSnackBar()
            } }
        })
    }

    override fun setupViews(context: Context) {
        binding.refresh.isRefreshing = true
        binding.refresh.setOnRefreshListener(viewModel::loadSchedule)

        adapter = ScheduleAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(SpaceItemDecorator(context, R.dimen.size_4dp))

        // Request schedule from Firestore
        viewModel.loadSchedule()
    }

    private fun showSnackBar() {
        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.no_auth, Snackbar.LENGTH_LONG).show()
    }

}
