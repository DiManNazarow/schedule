package ru.max.schedule.models

data class ScheduleResponseData(
    // User uuid
    var uuid: String,
    // Days with schedule of this user
    var days: List<Day>
) {

    // Empty constructor requires for auto convert from Firestore document
    constructor() : this("", emptyList())

    data class Day(
        // Day name
        var day: String,
        // List of time end subjects
        var schedule: List<Schedule>
    ) {

        // Empty constructor requires for auto convert from Firestore document
        constructor() : this("", emptyList())

        data class Schedule(
            // Time of subject
            var time: String,
            // Name of subject
            var subject: String
        ) {
            // Empty constructor requires for auto convert from Firestore document
            constructor() : this("", "")
        }

    }

}