package ru.max.schedule.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.max.schedule.R
import ru.max.schedule.databinding.LayoutNotificationViewHolderBinding

/**
 * Adapters for notifications
 */
class NotificationsAdapter : AbsRecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        NotificationViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NotificationViewHolder).setData(items[position].data as NotificationItemData)
    }

}

class NotificationViewHolder(parent: ViewGroup) : AbsViewHolder<NotificationItemData, LayoutNotificationViewHolderBinding>(
    R.layout.layout_notification_view_holder, parent
) {

    override fun fill() {
        binding.itemData = itemData
        binding.executePendingBindings()
    }

}

class NotificationItemData(
    val message: String
) : ListItemData()