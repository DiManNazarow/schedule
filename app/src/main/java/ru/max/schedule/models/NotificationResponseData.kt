package ru.max.schedule.models

data class NotificationResponseData(
    var uuid: String,
    var message: String
) {

    // Empty constructor requires for auto convert from Firestore document
    constructor() : this("", "")
}