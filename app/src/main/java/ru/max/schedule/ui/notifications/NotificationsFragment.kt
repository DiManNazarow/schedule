package ru.max.schedule.ui.notifications

import android.content.Context
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import ru.max.schedule.R
import ru.max.schedule.adapters.ListItem
import ru.max.schedule.adapters.NotificationsAdapter
import ru.max.schedule.adapters.SpaceItemDecorator
import ru.max.schedule.databinding.FragmentNotificationsBinding
import ru.max.schedule.ui.BaseFragment

class NotificationsFragment : BaseFragment<FragmentNotificationsBinding>() {

    private val viewModel: NotificationsViewModel by viewModel()

    private lateinit var adapter: NotificationsAdapter

    override fun getLayout(): Int = R.layout.fragment_notifications

    override fun setupViewModels(context: Context) {
        viewModel.items.observe(this, Observer { items ->
            items?.let {
                processEmpty(it)
                adapter.items = it
                binding.refresh.isRefreshing = false
            }
        })
        viewModel.noAuth.observe(this, Observer { noAuth ->
            noAuth?.let { if (noAuth) {
                binding.refresh.isRefreshing = false
                showSnackBar()
            } }
        })
    }

    override fun setupViews(context: Context) {
        binding.refresh.isRefreshing = true
        binding.refresh.setOnRefreshListener(viewModel::loadNotifications)

        adapter = NotificationsAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(SpaceItemDecorator(context, R.dimen.size_4dp))

        viewModel.loadNotifications()
    }

    private fun processEmpty(items: List<ListItem<*>>) {
        binding.recyclerView.isVisible = items.isNotEmpty()
        binding.empty.isVisible = items.isEmpty()
    }

    private fun showSnackBar() {
        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.no_auth, Snackbar.LENGTH_LONG).show()
    }

}
