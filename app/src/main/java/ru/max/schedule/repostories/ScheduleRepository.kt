package ru.max.schedule.repostories

import com.google.firebase.firestore.QuerySnapshot

/**
 * Repository for access schedule on Firestore
 */
interface ScheduleRepository {

    /**
     * Loads schedule from Firestore
     * @param uuid - user's uuid
     * @return - requested document
     */
    suspend fun loadSchedule(uuid: String): QuerySnapshot?

}