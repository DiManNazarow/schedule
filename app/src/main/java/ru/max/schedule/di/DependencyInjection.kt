package ru.max.schedule.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.app
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.max.schedule.repostories.NotificationsRepository
import ru.max.schedule.repostories.ScheduleRepository
import ru.max.schedule.repostories.impl.NotificationsRepositoryImpl
import ru.max.schedule.repostories.impl.ScheduleRepositoryImpl
import ru.max.schedule.ui.news.NewsViewModel
import ru.max.schedule.ui.notifications.NotificationsViewModel
import ru.max.schedule.ui.profile.ProfileViewModel
import ru.max.schedule.ui.schedule.ScheduleViewModel
import ru.max.schedule.useCases.LoadNotificationUseCase
import ru.max.schedule.useCases.LoadScheduleUseCase

val appModule = module {
    single { Firebase.app }
    single { FirebaseAuth.getInstance(get()) }
    single { Firebase.firestore(get()) }
}

val repositoryModule = module {
    single { ScheduleRepositoryImpl(get()) as ScheduleRepository }
    single { NotificationsRepositoryImpl(get()) as NotificationsRepository }
}

val useCaseModule = module {
    factory { LoadScheduleUseCase(get()) }
    factory { LoadNotificationUseCase(get()) }
}

val viewModelModule = module {
    viewModel { ScheduleViewModel(get(), get()) }
    viewModel { NewsViewModel() }
    viewModel { NotificationsViewModel(get(), get()) }
    viewModel { ProfileViewModel(get()) }
}