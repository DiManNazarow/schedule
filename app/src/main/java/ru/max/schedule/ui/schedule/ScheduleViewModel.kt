package ru.max.schedule.ui.schedule

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import ru.max.schedule.adapters.ListItem
import ru.max.schedule.adapters.ScheduleItemData
import ru.max.schedule.models.ScheduleResponseData
import ru.max.schedule.models.SubjectData
import ru.max.schedule.useCases.LoadScheduleUseCase

class ScheduleViewModel(
    private val loadScheduleUseCase: LoadScheduleUseCase,
    private val firebaseAuth: FirebaseAuth
) : ViewModel() {

    private val _noAuth = MutableLiveData<Boolean>()
    val noAuth: LiveData<Boolean> = _noAuth

    private val _items = MutableLiveData<List<ListItem<*>>>()
    val items: LiveData<List<ListItem<*>>> = _items

    fun loadSchedule() {
        viewModelScope.launch {
            // Gets auth user
            val user = firebaseAuth.currentUser
            if (user != null) {
                // If user auth load schedule for this one
                val data = loadScheduleUseCase.loadSchedule(user.uid)
                if (data != null) {
                    // If there are data create list items
                    createItems(data)
                }
            } else {
                // If no user show error notification
                _noAuth.value = true
            }
        }
    }

    /**
     * Create items list for recycler adapter
     */
    private fun createItems(schedule: ScheduleResponseData) {
        val items = ArrayList<ListItem<*>>()
        schedule.days.forEach { sc ->
            items.add(ListItem(ScheduleItemData(day = sc.day, subjects = sc.schedule.map { SubjectData(it.time, it.subject) })))
        }
        _items.value = items
    }

}