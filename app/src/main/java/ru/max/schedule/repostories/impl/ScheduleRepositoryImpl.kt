package ru.max.schedule.repostories.impl

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.max.schedule.await
import ru.max.schedule.repostories.ScheduleRepository

// Name of collection on Firestore
private const val SCHEDULE_COLLECTION = "schedule"

/**
 * Real implementation of [ScheduleRepository]
 */
class ScheduleRepositoryImpl(
    private val firestore: FirebaseFirestore
) : ScheduleRepository {

    override suspend fun loadSchedule(uuid: String): QuerySnapshot? = withContext(Dispatchers.IO) {
        return@withContext try {
            firestore.collection(SCHEDULE_COLLECTION).whereEqualTo("uuid", uuid).get().await()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

}