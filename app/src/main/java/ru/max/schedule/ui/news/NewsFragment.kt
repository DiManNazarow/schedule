package ru.max.schedule.ui.news

import android.content.Context
import androidx.lifecycle.Observer
import org.koin.android.viewmodel.ext.android.viewModel
import ru.max.schedule.R
import ru.max.schedule.databinding.FragmentNewsBinding
import ru.max.schedule.ui.BaseFragment

class NewsFragment : BaseFragment<FragmentNewsBinding>() {

    private val viewModel: NewsViewModel by viewModel()

    override fun getLayout(): Int = R.layout.fragment_news

    override fun setupViewModels(context: Context) {
        viewModel.text.observe(this, Observer {
            binding.textHome.text = it
        })
    }

}
