package ru.max.schedule.ui.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.Observer
import com.firebase.ui.auth.AuthUI
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import ru.max.schedule.R
import ru.max.schedule.databinding.FragmentProfileBinding
import ru.max.schedule.ui.BaseFragment

const val RC_SIGN_IN = 1234

/**
 * Fragment to perform sign in/out and show user info
 */
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val viewModel: ProfileViewModel by viewModel()

    override fun getLayout(): Int = R.layout.fragment_profile

    override fun setupViewModels(context: Context) {
        viewModel.startAuth.observe(this, Observer { state ->
            state?.let { if (it) startAuth() }
        })
    }

    override fun setupViews(context: Context) {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        binding.signInButton.setOnClickListener {
            startAuth()
        }
        binding.signOutButton.setOnClickListener {
            signOut()
        }
        // Calls viewModel's method to check auth user
        viewModel.checkAuth()
    }

    /**
     * Activates Sing In process
     */
    private fun startAuth() {
        // Creates list of available Sing In options
        // For now Google and Email auth supported
        val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().setAllowNewAccounts(true).build(),
            AuthUI.IdpConfig.GoogleBuilder().build()
        )
        // Launch framework's activity to perform Sign In
        startActivityForResult(
            AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.AppTheme)
                .build(), RC_SIGN_IN
        )
    }

    private fun signOut() {
        // Calls framework's Sign Out methods
        AuthUI.getInstance().signOut(requireContext()).addOnCompleteListener {
            // In case of success show Sign In UI
            viewModel.setSignIn()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                // If Sign In was succeeded calls viewModel's method to check auth user
                viewModel.checkAuth()
            } else {
                // Otherwise shows failure message
                Snackbar.make(binding.root, R.string.sign_in_error, Snackbar.LENGTH_LONG).show()
            }
        }
    }

}