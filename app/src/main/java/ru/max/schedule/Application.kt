package ru.max.schedule

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.max.schedule.di.appModule
import ru.max.schedule.di.repositoryModule
import ru.max.schedule.di.useCaseModule
import ru.max.schedule.di.viewModelModule

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Application)
            modules(listOf(appModule, repositoryModule, useCaseModule, viewModelModule))
        }
    }

}