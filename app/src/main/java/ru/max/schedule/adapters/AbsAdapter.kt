package ru.max.schedule.adapters

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DimenRes
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class AbsRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    open var items: List<ListItem<*>> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = items[position].viewHolderType

}

abstract class AbsViewHolder<T : ListItemData, ViewBinding : ViewDataBinding> :
    RecyclerView.ViewHolder {

    val binding: ViewBinding

    protected lateinit var itemData: T

    private constructor(binding: ViewBinding) : super(binding.root) {
        this.binding = binding
    }

    constructor(@LayoutRes layoutId: Int, parent: ViewGroup) : this(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutId,
            parent,
            false
        )
    )

    protected abstract fun fill()

    fun setData(itemData: T) {
        this.itemData = itemData
        fill()
    }

    protected fun getContext(): Context = itemView.context

}

data class ListItem<T : ListItemData>(val data: T, val viewHolderType: Int = 0)

open class ListItemData

class SpaceItemDecorator(context: Context, @DimenRes marginRes: Int) : RecyclerView.ItemDecoration() {

    private val margin = context.resources.getDimensionPixelSize(marginRes)

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (parent.getChildAdapterPosition(view) >= 0) {
            outRect.top = margin
        }
        if (parent.getChildAdapterPosition(view) == parent.adapter?.itemCount?.minus(1) ?: 0) {
            outRect.bottom = margin
        }
    }

}