package ru.max.schedule.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.max.schedule.R
import ru.max.schedule.databinding.LayoutScheduleViewHolderBinding
import ru.max.schedule.models.SubjectData

/**
 * Adapter for schedule list
 */
class ScheduleAdapter : AbsRecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ScheduleViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ScheduleViewHolder).setData(items[position].data as ScheduleItemData)
    }

}

class ScheduleViewHolder(parent: ViewGroup) : AbsViewHolder<ScheduleItemData, LayoutScheduleViewHolderBinding>(
    R.layout.layout_schedule_view_holder, parent
) {

    override fun fill() {
        binding.itemData = itemData
        binding.executePendingBindings()
    }

}

class ScheduleItemData(
    val day: String,
    val subjects: List<SubjectData>
) : ListItemData()
