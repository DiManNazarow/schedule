package ru.max.schedule.useCases

import com.google.firebase.firestore.ktx.toObject
import ru.max.schedule.models.ScheduleResponseData
import ru.max.schedule.repostories.ScheduleRepository

/**
 * Use case which load schedule from Firestore
 * and convert to internal data model
 * @param scheduleRepository - repository for access schedule on Firestore
 */
class LoadScheduleUseCase(
    private val scheduleRepository: ScheduleRepository
) {

    /**
     * Loads and convert data
     */
    suspend fun loadSchedule(uuid: String): ScheduleResponseData? {
        // Gets data for firestore
        val data = scheduleRepository.loadSchedule(uuid)
        data?.documents?.forEach { doc ->
            // It should be only one row from request which has to be converted
            return doc.toObject<ScheduleResponseData>() ?: throw IllegalStateException("Cannot convert document to Schedule model")
        }
        return null
    }

}