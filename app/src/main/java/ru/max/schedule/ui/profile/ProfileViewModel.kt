package ru.max.schedule.ui.profile

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class ProfileViewModel(
    private val firebaseAuth: FirebaseAuth
) :  ViewModel() {

    val name = ObservableField<String>()

    val email = ObservableField<String>()

    val signInVisibility = ObservableField<Int>()

    val signOutVisibility = ObservableField<Int>()

    private val _startAuth = MutableLiveData<Boolean>()
    val startAuth: LiveData<Boolean> = _startAuth

    init {
        // From start show Sign In UI
        setSignIn()
    }

    /**
     * Show Sing In UI
     */
    fun setSignIn() {
        name.set("Your name")
        email.set("example@mail.com")
        signInVisibility.set(View.VISIBLE)
        signOutVisibility.set(View.GONE)
    }

    /**
     * Checks FirebaseAuth to auth user
     */
    fun checkAuth() {
        val user = firebaseAuth.currentUser
        if (user != null) {
            // If there is auth user
            // Show sign out UI
            setSignOut()
            // And user's info
            setUserInfo(user)
        } else {
            setSignIn()
            // If no user start auth process
            _startAuth.value = true
        }
    }

    /**
     * Shows Sign Out UI
     */
    private fun setSignOut() {
        signInVisibility.set(View.GONE)
        signOutVisibility.set(View.VISIBLE)
    }

    /**
     * Shows user's info
     * @param - auth user
     */
    private fun setUserInfo(user: FirebaseUser) {
        name.set(user.displayName)
        email.set(user.email)
    }

}