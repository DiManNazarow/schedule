package ru.max.schedule.repostories

import com.google.firebase.firestore.QuerySnapshot

/**
 * Repository for access notifications on Firestore
 */
interface NotificationsRepository {

    /**
     * Loads notifications from Firestore
     * @param uuid - user's uuid
     * @return - requested document
     */
    suspend fun loadNotifications(uuid: String): QuerySnapshot?

}