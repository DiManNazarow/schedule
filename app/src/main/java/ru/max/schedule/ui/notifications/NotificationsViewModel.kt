package ru.max.schedule.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import ru.max.schedule.adapters.ListItem
import ru.max.schedule.adapters.NotificationItemData
import ru.max.schedule.models.NotificationResponseData
import ru.max.schedule.useCases.LoadNotificationUseCase

class NotificationsViewModel(
    private val loadNotificationUseCase: LoadNotificationUseCase,
    private val firebaseAuth: FirebaseAuth
) : ViewModel() {

    private val _noAuth = MutableLiveData<Boolean>()
    val noAuth: LiveData<Boolean> = _noAuth

    private val _items = MutableLiveData<List<ListItem<*>>>()
    val items: LiveData<List<ListItem<*>>> = _items

    fun loadNotifications() {
        viewModelScope.launch {
            // Gets auth user
            val user = firebaseAuth.currentUser
            if (user != null) {
                // If user auth load notifications for this one
                val data = loadNotificationUseCase.loadNotifications(user.uid)
                createItems(data)
            } else {
                // If no user show error notifications
                _noAuth.value = true
            }
        }
    }

    /**
     * Create items list for recycler adapter
     */
    private fun createItems(notifications: List<NotificationResponseData>) {
        val items = ArrayList<ListItem<*>>()
        notifications.forEach { n ->
            items.add(ListItem(NotificationItemData(n.message)))
        }
        _items.value = items
    }

}