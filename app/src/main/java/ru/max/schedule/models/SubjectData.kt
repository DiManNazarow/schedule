package ru.max.schedule.models

/**
 * Data class for recycler view
 */
data class SubjectData(
    // Time of subject
    val time: String,
    // Name of subject
    val subject: String
)