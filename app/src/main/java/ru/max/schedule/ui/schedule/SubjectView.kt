package ru.max.schedule.ui.schedule

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import ru.max.schedule.R

class SubjectView : FrameLayout {

    private val time: TextView by lazy { findViewById<TextView>(R.id.time) }

    private val subject: TextView by lazy { findViewById<TextView>(R.id.subject) }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.layout_subject_view, this, true)
    }

    fun setTime(time: String) {
        this.time.text = time
    }

    fun setSubject(subject: String) {
        this.subject.text = subject
    }

}