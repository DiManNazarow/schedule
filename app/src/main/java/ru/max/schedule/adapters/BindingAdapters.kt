package ru.max.schedule.adapters

import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import ru.max.schedule.models.SubjectData
import ru.max.schedule.ui.schedule.SubjectView

@BindingAdapter("subjects")
fun setSubjects(container: LinearLayout, subjects: List<SubjectData>) {
    container.removeAllViews()
    subjects.forEach { data ->
        container.addView(SubjectView(container.context).apply {
            setTime(data.time); setSubject(data.subject)
        })
    }
}
